//
//  AddNotificationController.swift
//  swift-SATime
//
//  Created by Zemskix Denis prod. First Bit on 14/02/17.
//  Copyright (c) 2017 Zemskix Denis. All rights reserved.
//

import UIKit
import CoreData


protocol InputCellProtocol {
    func tableView(tableView: UITableView, didDoneAtIndexPath indexPath: NSIndexPath)
}

class InputCell: UITableViewCell {
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var inputField: UITextField!

    @IBAction func done(sender: AnyObject) {
        let indexPath = self.superTableView().indexPathForCell(self)
        (self.superTableView().delegate as AddNotificationController as InputCellProtocol).tableView(self.superTableView(), didDoneAtIndexPath: indexPath!)
    }
    
}

class PickerCell: UITableViewCell {
    @IBOutlet weak var pickerView: UIPickerView!
}

class RuleCell: UITableViewCell, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var ruleField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    var amount : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        ruleField.inputView = picker
        ruleField.text = RuleType(rawValue: 1)?.description
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        ruleField.text = RuleType(rawValue: row+1)?.description

        let indexPath = self.superTableView().indexPathForCell(self)
        (self.superTableView().delegate as AddNotificationController as InputCellProtocol).tableView(self.superTableView(), didDoneAtIndexPath: indexPath!)
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return RuleType.numberOfMembers
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return RuleType(rawValue: row+1)?.description
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        amount = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)

        let indexPath = self.superTableView().indexPathForCell(self)
        (self.superTableView().delegate as AddNotificationController as InputCellProtocol).tableView(self.superTableView(), didDoneAtIndexPath: indexPath!)

        return true
    }
    
}

class AddNotificationController: UITableViewController, InputCellProtocol, UISearchControllerDelegate, UISearchResultsUpdating {

    enum SectionIndexes: Int {
        case Parameter = 0
        case Analytic
        case Rule
        
        static var numberOfMembers : Int {
            return 3
        }
    }
    
    var notification: Notification!
    private var contextObserver: AnyObject?
    override func viewDidLoad() {
        super.viewDidLoad()

        if notification == nil {
            notification = NSEntityDescription.insertNewObjectForEntityForName("Notification", inManagedObjectContext: appDelegate.managedObjectContext!) as Notification
            notification.rule = NSNumber(integer: 1)
        }
        

        contextObserver = NSNotificationCenter.defaultCenter().addObserverForName(NSManagedObjectContextObjectsDidChangeNotification, object:appDelegate.managedObjectContext, queue:NSOperationQueue.mainQueue(), usingBlock:{ (note: NSNotification!) -> Void in
            self.tableView.beginUpdates()
            self.tableView.reloadSections(NSIndexSet(indexesInRange: NSMakeRange(SectionIndexes.Parameter.rawValue, 2)), withRowAnimation: UITableViewRowAnimation.Automatic)
            self.tableView.endUpdates()
        })
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        appDelegate.managedObjectContext?.rollback()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(contextObserver!)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return SectionIndexes.numberOfMembers
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionIndex = SectionIndexes(rawValue: section) {
            switch sectionIndex {
            case .Parameter :
                return 1
            case .Analytic where notification.valueForKey("parameter") != nil :
                return notification.parameter.analytics.count
            case .Rule :
                return 1
            default:
                return 0
            }
        }
        return 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let sectionIndex = SectionIndexes(rawValue: indexPath.section) {
            switch sectionIndex {
            case .Parameter :
                let cell = tableView.dequeueReusableCellWithIdentifier("parameter", forIndexPath: indexPath) as UITableViewCell
                cell.textLabel?.text = "Параметр"
                if notification.valueForKey("parameter") != nil {
                    cell.detailTextLabel?.text = notification.parameter.name
                } else {
                    cell.detailTextLabel?.text = "Не задано"
                }
                return cell
            case .Analytic :
                let cell = tableView.dequeueReusableCellWithIdentifier("analytic", forIndexPath: indexPath) as UITableViewCell
                if let analytics = notification.parameter.analytics.allObjects as? [Analytic] {
                    let analytic = (analytics.sorted({ (analyticValue1, analyticValue2) -> Bool in
                        return analyticValue1.name > analyticValue2.name
                    }) as [Analytic])[indexPath.row]
                    
                    cell.textLabel?.text = analytic.name
                    
                    if let condition = (notification.conditions.allObjects as [Condition]).filter({ ($0 as Condition).analyticValue.analytic == analytic }).first {
                        cell.detailTextLabel?.text = condition.analyticValue.value
                    } else {
                        cell.detailTextLabel?.text = "Не задано"
                    }
                }
                return cell
            case .Rule :
                let cell = tableView.dequeueReusableCellWithIdentifier("rule", forIndexPath: indexPath) as RuleCell
                cell.ruleField.text = RuleType(rawValue: notification.rule.integerValue)?.description
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("", forIndexPath: indexPath) as UITableViewCell // error generator
            return cell
        }

    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let sectionIndex = SectionIndexes(rawValue: indexPath.section) {
            switch sectionIndex {
            case .Parameter where notification.valueForKey("parameter") != nil:
                return true
            case .Analytic:
                if let analytics = notification.parameter.analytics.allObjects as? [Analytic] {
                    let analytic = (analytics.sorted({ (analyticValue1, analyticValue2) -> Bool in
                        return analyticValue1.name > analyticValue2.name
                    }) as [Analytic])[indexPath.row]
                    
                    if let condition = (notification.conditions.allObjects as [Condition]).filter({ ($0 as Condition).analyticValue.analytic == analytic }).first {
                        return true
                    }
                }
                return false
            default:
                return false
            }
        }
        return false
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {

            if let sectionIndex = SectionIndexes(rawValue: indexPath.section) {
                switch sectionIndex {
                case .Parameter :
                    if notification.valueForKey("parameter") != nil {
                        appDelegate.managedObjectContext?.deleteObject(notification.parameter)
                        for condition in notification.conditions.allObjects as [Condition] {
                            appDelegate.managedObjectContext?.deleteObject(condition)
                        }

                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            self.tableView.beginUpdates()
                            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
                            self.tableView.reloadSections(NSIndexSet(index: SectionIndexes.Analytic.rawValue), withRowAnimation: UITableViewRowAnimation.Fade)
                            self.tableView.endUpdates()
                        })
                    }
                case .Analytic :
                    if let analytics = notification.parameter.analytics.allObjects as? [Analytic] {
                        let analytic = (analytics.sorted({ (analyticValue1, analyticValue2) -> Bool in
                            return analyticValue1.name > analyticValue2.name
                        }) as [Analytic])[indexPath.row]
                        
                        if let condition = (notification.conditions.allObjects as [Condition]).filter({ ($0 as Condition).analyticValue.analytic == analytic }).first {
                            appDelegate.managedObjectContext?.deleteObject(condition)
                            
                            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                                self.tableView.beginUpdates()
                                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
                                self.tableView.endUpdates()
                            })
                        }
                    }
                default:
                    return
                }
            }
            
        } else if editingStyle == .Insert {

        }    
    }


    func tableView(tableView: UITableView, didDoneAtIndexPath indexPath: NSIndexPath) {
        if let sectionIndex = SectionIndexes(rawValue: indexPath.section) {
            switch sectionIndex {
            case .Rule :
                let cell = tableView.cellForRowAtIndexPath(indexPath) as RuleCell
                if let picker = cell.ruleField.inputView as? UIPickerView {
                    notification.rule = NSNumber(integer: picker.selectedRowInComponent(0) + 1)
                }
                notification.amount = NSNumber(integer: (cell.amount as NSString).integerValue)
            default:
                return
            }
        }
 
    }
    
    var searchController : UISearchController!
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let sectionIndex = SectionIndexes(rawValue: indexPath.section) {
            switch sectionIndex {
            case .Parameter :
                searchController = UISearchController(searchResultsController: ParameterPicker())
                searchController.delegate = self
                searchController.dimsBackgroundDuringPresentation = true // default is YES
                searchController.searchBar.delegate = searchController.searchResultsController as ParameterPicker!

                self.presentViewController(searchController, animated: true, completion: { () -> Void in
                    
                })
            case .Analytic :
                if let analytics = notification.parameter.analytics.allObjects as? [Analytic] {
                    let analytic = (analytics.sorted({ (analyticValue1, analyticValue2) -> Bool in
                        return analyticValue1.name > analyticValue2.name
                    }) as [Analytic])[indexPath.row]
                    
                    searchController = UISearchController(searchResultsController: AnalyticValuePicker(analytic: analytic))
                    searchController.delegate = self
                    searchController.dimsBackgroundDuringPresentation = true // default is YES
                    searchController.searchBar.delegate = searchController.searchResultsController as AnalyticValuePicker!
                    
                    self.presentViewController(searchController, animated: true, completion: { () -> Void in
                        
                    })
                }
            default:
                return
            }
        }

    }

    
    func presentSearchController(searchController: UISearchController) {
        //NSLog(__FUNCTION__)
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //NSLog(__FUNCTION__)
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //NSLog(__FUNCTION__)
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //NSLog(__FUNCTION__)
        if let parameterPicker = searchController.searchResultsController as? ParameterPicker {
            if let indexPath = parameterPicker.tableView.indexPathForSelectedRow() {
                let parameter = parameterPicker.fetchedResultsController.objectAtIndexPath(indexPath) as Parameter
                notification.parameter = parameter
                for condition in notification.conditions.allObjects as [Condition] {
                    appDelegate.managedObjectContext?.deleteObject(condition)
                }
            }
        }
        if let analyticValuePicker = searchController.searchResultsController as? AnalyticValuePicker {
            if let indexPath = analyticValuePicker.tableView.indexPathForSelectedRow() {
                let analyticValue = analyticValuePicker.fetchedResultsController.objectAtIndexPath(indexPath) as AnalyticValue
                var conditions = (notification.conditions.allObjects as [Condition]).filter({ ($0 as Condition).analyticValue.analytic.name == analyticValue.analytic.name })
                var condition : Condition!
                if conditions.count == 0 {
                    condition = NSEntityDescription.insertNewObjectForEntityForName("Condition", inManagedObjectContext: appDelegate.managedObjectContext!) as Condition
                    condition.notification = notification
                } else {
                    condition = conditions.first
                }
                condition.analyticValue = analyticValue
            }
        }
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        if let indexPath = self.tableView.indexPathForSelectedRow() {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.tableView.beginUpdates()
            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            self.tableView.reloadSections(NSIndexSet(index: SectionIndexes.Analytic.rawValue), withRowAnimation: UITableViewRowAnimation.Automatic)
            self.tableView.endUpdates()
        }
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
    }

    // MARK: - Navigation
    
    // MARK: - Local interface
    
    @IBAction func save(sender: AnyObject) {
        if notification.valueForKey("identificator") != nil {
            
        } else {
            var error: NSError? = nil
            if notification.validateForInsert(&error) {
                let createNotification = Loader.apiNotificationNew(NSURLSession.sharedSession(), notification: notification, completionHandler: {() -> Void in
                    
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        if let navigationController = self.navigationController {
                            navigationController.popViewControllerAnimated(true)
                        }
                    })
                })
                createNotification.resume()
            } else {
                println(error!.localizedDescription)
            }
        }
    }
    
}
