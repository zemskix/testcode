//
//  Loader.swift
//  swift-SATime
//
//  Created by Zemskix Denis prod. First Bit on 14/02/17.
//  Copyright (c) 2017 Zemskix Denis. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Loader: NSObject {

    class func apiTaskWithLogin(session: NSURLSession, login: String!, password: String!, completionHandler: (Bool -> Bool)?) -> NSURLSessionDataTask {

        let task = session.dataTaskWithRequest(NSMutableURLRequest(login: login, password: password), completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) in
            
            var reason = ""
            

                // Download error
                if error == nil {
                    switch (response as NSHTTPURLResponse).statusCode {
                    case 404:
                        reason = "Email or password is incorrect"
                    case let code where code <= 200:
                        reason = ""
                    default:
                        reason = "Something went wrong"
                    }
                } else {
                    if error.localizedRecoverySuggestion != nil {
                        reason = error.localizedRecoverySuggestion!
                    } else if error.localizedFailureReason != nil {
                        reason = error.localizedFailureReason!
                    } else {
                        reason = error.localizedDescription.componentsSeparatedByString("(").first!
                    }
                }

            if (data != nil) {
                
                if let path: String = appDelegate.applicationDocumentsDirectory.path? {
                    let fullPath = path.stringByAppendingString("/login.json")
                    data.writeToFile(fullPath, atomically: true)
                    println(fullPath)
                }
                
                var jsonError: NSError?
                
                if let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError) as? NSDictionary {
                    println(json)
                    
                    let context = NSManagedObjectContext()
                    context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
                    context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                    
                    // Set all users as non current
                    if (NSClassFromString("NSBatchUpdateRequest") != nil) {
                        let batch = NSBatchUpdateRequest(entityName: "User")
                        batch.predicate = NSPredicate(format: "isCurrent == true")
                        batch.propertiesToUpdate = ["isCurrent": false]
                        let batchResult = context.executeRequest(batch, error: nil)
                    } else {
                        let fetchAllUsers = NSFetchRequest(entityName: "User")
                        for user in context.executeFetchRequest(fetchAllUsers, error: nil) as [User] {
                            user.isCurrent = false
                        }
                    }
                    
                    // Get current user or create new
                    let user = NSEntityDescription.userForLogin(login, inManagedObjectContext: context)
                    user.applicationVersion = NSNumber(integer:(json["ApplicationVersion"] as NSNumber).integerValue)
                    user.authToken = json["AuthToken"] as String
                    user.isCurrent = true
                    
                    var error: NSError? = nil
                    if context.hasChanges && !context.save(&error) {
                        println(error)
                    }

                }
            }
            
            if completionHandler != nil {
                if completionHandler!(reason == "") {
                    if reason != "" {
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            let alert = UIAlertView(title: "Message", message: reason, delegate: nil, cancelButtonTitle: "Accept")
                            alert.show()
                        })
                    }
                }
            }
            
        })
        
        return task
    }
    
    class func apiMetric(session: NSURLSession, lastMetricID: NSNumber!) -> (task: NSURLSessionDownloadTask, userID: NSManagedObjectID)? {
        let context = NSManagedObjectContext()
        context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        let userRequest = NSFetchRequest(entityName: "User")
        userRequest.predicate = NSPredicate(format: "isCurrent == true")
        if let users = context.executeFetchRequest(userRequest, error: nil) as? [User] {
            if users.count == 0 {
                return nil
            }
            
            let user = users.first as User!
            let userID = user.objectID
            
            var task : NSURLSessionDownloadTask
            if lastMetricID != nil {
                task = session.downloadTaskWithRequest(NSMutableURLRequest(token: user.authToken, fromMetricID: lastMetricID))
            } else {
                let metrics = (user.parameters.allObjects as NSArray).valueForKeyPath("@distinctUnionOfSets.metrics") as NSArray!
                let maxMetricID = metrics.valueForKeyPath("@max.identificator") as NSNumber!
                
                task = session.downloadTaskWithRequest(NSMutableURLRequest(token: user.authToken, fromMetricID: maxMetricID))
            }
            
            return (task, userID)
        }
        return nil
    }
    
    class func apiRegister(session: NSURLSession, email: String, password: String, completionHandler: (Bool -> Void)?) -> NSURLSessionDataTask {
        let task = session.dataTaskWithRequest(NSMutableURLRequest(email: email, password: password), completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) in
            
            var reason = ""
            if error == nil {
                println((response as NSHTTPURLResponse).statusCode)
                
                switch (response as NSHTTPURLResponse).statusCode {
                case 400:
                    reason = "Email fileld has an invalid format"
                case 409:
                    reason = "Email is already is use"
                case let code where code <= 200:
                    reason = ""
                default:
                    reason = "Something went wrong"
                }
            } else {
                reason = error.localizedDescription
            }

            if completionHandler != nil {
                completionHandler!(reason == "")
            }

            if reason != "" {
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    let alert = UIAlertView(title: "Message", message: reason, delegate: nil, cancelButtonTitle: "Accept")
                    alert.show()
                })
            }
        })
        
        return task
    }

    class func apiPassword(session: NSURLSession, oldPassword: String, password: String, completionHandler: (Void -> Void)?) -> NSURLSessionDataTask {
        let context = NSManagedObjectContext()
        context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        let userRequest = NSFetchRequest(entityName: "User")
        userRequest.predicate = NSPredicate(format: "isCurrent == true")
        let users = context.executeFetchRequest(userRequest, error: nil) as [User]
        let user = users.first as User!
        let login = user.login
        
        let task = session.dataTaskWithRequest(NSMutableURLRequest(login: login, password: password, oldPassword: oldPassword), completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) in
            
            var reason = ""
            if error == nil {
                println((response as NSHTTPURLResponse).statusCode)
                
                switch (response as NSHTTPURLResponse).statusCode {
                case 500:
                    reason = "Password is wrong"
                case let code where code <= 200:
                    reason = "Password was changed successfully"
                default:
                    reason = "Something went wrong"
                }
            } else {
                reason = error.localizedDescription
            }
            
            if (data != nil) {
                
                if let path: String = appDelegate.applicationDocumentsDirectory.path? {
                    let fullPath = path.stringByAppendingString("/password.json")
                    data.writeToFile(fullPath, atomically: true)
                    println(fullPath)
                }
                
                var jsonError: NSError?
                
                if let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError) as? NSDictionary {
                    println(json)
                    
                    let context = NSManagedObjectContext()
                    context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
                    context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                    
                    // Get current user or create new
                    let user = NSEntityDescription.userForLogin(login, inManagedObjectContext: context)
                    user.applicationVersion = NSNumber(integer:(json["ApplicationVersion"] as NSNumber).integerValue)
                    user.authToken = json["AuthToken"] as String
                    
                    var error: NSError? = nil
                    if context.hasChanges && !context.save(&error) {
                        println(error)
                    }
                }
            }

            if completionHandler != nil {
                completionHandler!()
            }
            
            if reason != "" {
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    let alert = UIAlertView(title: "Message", message: reason, delegate: nil, cancelButtonTitle: "Accept")
                    alert.show()
                })
            }
        })
        
        return task
    }

    class func apiTrial(session: NSURLSession, email: String, password: String, name: String, phone: String, completionHandler: (Bool -> Void)?) -> NSURLSessionDataTask {
        let task = session.dataTaskWithRequest(NSMutableURLRequest(email: email, password: password, name: name, phone: phone), completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) in
            
            var reason = ""
            if error == nil {
                println((response as NSHTTPURLResponse).statusCode)
                
                switch (response as NSHTTPURLResponse).statusCode {
                case 400:
                    reason = "Phone fileld has an invalid format"
                case 409:
                    reason = "Email is already is use"
                case let code where code <= 200:
                    reason = "Password changed successfully"
                default:
                    reason = "Something went wrong"
                }
            } else {
                reason = error.localizedDescription
            }
            
            if completionHandler != nil {
                completionHandler!(reason == "")
            }
            
            if reason != "" {
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    let alert = UIAlertView(title: "Message", message: reason, delegate: nil, cancelButtonTitle: "Accept")
                    alert.show()
                })
            }
        })
        
        return task
    }
    
    class func apiNotifications(session: NSURLSession, completionHandler: ([[String: AnyObject]]! -> Void)) -> NSURLSessionDataTask {
        let context = NSManagedObjectContext()
        context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        let userRequest = NSFetchRequest(entityName: "User")
        userRequest.predicate = NSPredicate(format: "isCurrent == true")
        let users = context.executeFetchRequest(userRequest, error: nil) as [User]
        let user = users.first as User!
        let hash = user.authToken
        
        let task = session.dataTaskWithRequest(NSMutableURLRequest.notifications(hash), completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) in
            
            if (data != nil) {
                
                if let path: String = appDelegate.applicationDocumentsDirectory.path? {
                    let fullPath = path.stringByAppendingString("/notifications.json")
                    data.writeToFile(fullPath, atomically: true)
                    println(fullPath)
                }
                
                var jsonError: NSError?
                
                if let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError) as? [[String: AnyObject]] {
                    completionHandler(json)
                }
            } else {
                completionHandler(nil)
            }
            
        })
        
        return task
    }

    class func apiNotificationNew(session: NSURLSession, notification: Notification, completionHandler: (() -> Void)) -> NSURLSessionDataTask {
        let context = NSManagedObjectContext()
        context.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        let userRequest = NSFetchRequest(entityName: "User")
        userRequest.predicate = NSPredicate(format: "isCurrent == true")
        let users = context.executeFetchRequest(userRequest, error: nil) as [User]
        let user = users.first as User!
        let hash = user.authToken
        
        let task = session.dataTaskWithRequest(NSMutableURLRequest.createNotification(hash, notification: notification), completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) in
            
            println((response as NSHTTPURLResponse).statusCode)

            if (data != nil) {
                
                if let path: String = appDelegate.applicationDocumentsDirectory.path? {
                    let fullPath = path.stringByAppendingString("/notification_new.json")
                    data.writeToFile(fullPath, atomically: true)
                    println(fullPath)
                }
                
            } else {
            }
            
            completionHandler()
        })
        
        return task
    }
    
}



